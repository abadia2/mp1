import argparse
import io
import math
import multiprocessing
import os
import socket
import timeit
from datetime import datetime
from multiprocessing import Process, Queue
from typing import Union, Dict, List

from message import (create_msg_from_bytes, create_msg_from_string, MESSAGE_SIZE, msg_body_from_bytes,
                     msg_flags_from_bytes, msg_length_from_bytes, PORT)
from flags import Flags, is_flag_set

_CURRENT_DATETIME: str = datetime.now().strftime("%Y-%m-%d-%H.%M.%S")

_HOST_NAMES: Dict[str, int] = {
    "fa23-cs425-7801.cs.illinois.edu": 1,
    "fa23-cs425-7802.cs.illinois.edu": 2,
    "fa23-cs425-7803.cs.illinois.edu": 3,
    "fa23-cs425-7804.cs.illinois.edu": 4,
    "fa23-cs425-7805.cs.illinois.edu": 5,
    "fa23-cs425-7806.cs.illinois.edu": 6,
    "fa23-cs425-7807.cs.illinois.edu": 7,
    "fa23-cs425-7808.cs.illinois.edu": 8,
    "fa23-cs425-7809.cs.illinois.edu": 9,
    "fa23-cs425-7810.cs.illinois.edu": 10
}


def get_vm_ips(machine: str) -> str:
    return socket.gethostbyname(machine)


def get_log_file_path(machine: str) -> str:
    return f"/cs425/{_HOST_NAMES.get(machine)}"


def disconnect(conn: socket.socket) -> None:
    conn.send(create_msg_from_bytes(Flags.DISCONNECT))


def fetch_distributed_logs(grep_query: str, machine: str, timeout: int, result_queue: Queue, port: int) -> None:
    host_ip = socket.gethostbyname(machine)
    source = f"[{os.getpid()}, {machine}, {host_ip}]"
    print(f"{source} Client STARTED on port {port}")

    # Output path for search results
    path_to_output_file = f"results/{_CURRENT_DATETIME}/{machine}-result.log"
    os.makedirs(os.path.dirname(path_to_output_file), exist_ok=True)

    # Connect to Socket Server
    try:
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.settimeout(timeout)
        client.connect((host_ip, port))
        print(f"{source} Client RUNNING")
    except Exception as e:
        print(f"{source} Client FAILED {e}")
        result_queue.put({
            "machine": machine,
            "host_ip": host_ip,
            "line_count": math.nan
        })
        return

    # Send grep query to socket server to get logs
    client.send(create_msg_from_string(Flags.LOG_REQUEST, grep_query))

    # Keep file open while listening to socket
    with open(path_to_output_file, "w+") as output_file:

        # Since the number of bytes read from the socket vary, store bytes in a buffer until a message can be read
        buffer = io.BytesIO()

        while True:

            # Read bytes from socket until a message can be read from the buffer
            while buffer.tell() < MESSAGE_SIZE:
                _bytes = client.recv(MESSAGE_SIZE)
                buffer.write(_bytes)

            _len = buffer.tell()
            buffer.seek(0)
            byte_msg = buffer.read(MESSAGE_SIZE)

            if _len > MESSAGE_SIZE:
                overflow_len = _len - MESSAGE_SIZE
                extra = buffer.read(overflow_len)
                buffer.seek(0)
                buffer.write(extra)
            else:
                buffer.seek(0)

            # First byte contains flags indicating the type of message where only one flag should be set at a time
            msg_flags: int = msg_flags_from_bytes(byte_msg)

            # If multiple flags are set, be careful of the order
            if is_flag_set(msg_flags, Flags.LOG_RESPONSE):
                try:
                    msg_length: int = msg_length_from_bytes(byte_msg)
                    msg_body: str = msg_body_from_bytes(byte_msg, msg_length)
                    output_file.write(msg_body)
                except Exception as e:
                    print(f"{source} {e}")
                    disconnect(client)
            elif is_flag_set(msg_flags, Flags.DISCONNECT):
                msg_length: int = msg_length_from_bytes(byte_msg)
                line_count: str = msg_body_from_bytes(byte_msg, msg_length)
                print(f"{source} Server finished query with {line_count} matches")
                disconnect(client)
                break

    client.close()
    result_queue.put({
        "machine": machine,
        "host_ip": host_ip,
        "line_count": line_count
    })


def main() -> None:

    # Command line
    parser = argparse.ArgumentParser(description="Distributed Log Grep Client")
    parser.add_argument("query", type=str, help="Grep Query")
    parser.add_argument("--timeout", type=int, help="Timeout to wait for machine connections")
    parser.add_argument("--vms", type=str, nargs="+", help="List of host ips")
    parser.add_argument("--port", help="Override the default port the client listens")
    args = parser.parse_args()
    grep_query = args.query
    timeout = args.timeout or 5
    vms = args.vms or _HOST_NAMES
    port = int(args.port or PORT)

    # Fetch logs from each host in a separate thread
    result_queue: Queue = multiprocessing.Queue()
    p: List[Process] = []
    for vm in vms:
        p.append(Process(target=fetch_distributed_logs, args=(grep_query, vm, timeout, result_queue, port)))
    _ = [process.start() for process in p]
    _ = [process.join() for process in p]

    print("\n\nDistributed Grep")
    print("-----------------------------------------------------------------------------------------------------------")
    total = 0
    total_without_nan = 0
    while not result_queue.empty():
        result: Dict[str, Union[str, int, math.nan]] = result_queue.get()
        line_count = float(result.get("line_count"))
        if not math.isnan(line_count):
            total_without_nan += line_count
        total += line_count
        print(f"[{result.get('machine')} - {result.get('host_ip')}] Line Count: {line_count}")
    print(f"\nTotal Line Count: {total}\n")
    print(f"Total Line Count without NaN: {total_without_nan}\n")


if __name__ == "__main__":
    # python client.py "grep 'ERROR'" --timeout 5 --port 42069 --vms fa23-cs425-7807.cs.illinois.edu fa23-cs425-7808.cs.illinois.edu fa23-cs425-7809.cs.illinois.edu fa23-cs425-7810.cs.illinois.edu
    # python client.py "grep 'GET'" --timeout 5 --port 42069 --vms fa23-cs425-7807.cs.illinois.edu fa23-cs425-7808.cs.illinois.edu fa23-cs425-7809.cs.illinois.edu fa23-cs425-7810.cs.illinois.edu
    # python client.py "grep '17/Aug/2022:19:39:21'" --timeout 5 --port 42069 --vms fa23-cs425-7807.cs.illinois.edu fa23-cs425-7808.cs.illinois.edu fa23-cs425-7809.cs.illinois.edu fa23-cs425-7810.cs.illinois.edu
    print(f"Finished in {timeit.timeit(main, number=1)} seconds. See /results/{_CURRENT_DATETIME}/<ip>-result.log")
