import enum


class Flags(enum.Enum):
    EMPTY = 0b00000000
    DISCONNECT = 0b00000001
    LOG_REQUEST = 0b00000010
    LOG_RESPONSE = 0b00000100


def is_flag_set(flags: int, msg_type: Flags) -> bool:
    return flags & msg_type.value > 0


def set_flag(curr: int, msg_type: Flags) -> int:
    return curr | msg_type.value


def unset_flag(flags: int, msg_type: Flags) -> int:
    return flags & ~msg_type.value
