import os
import random
import socket
import time

_VMS = {
    "fa23-cs425-7801.cs.illinois.edu": 1,
    "fa23-cs425-7803.cs.illinois.edu": 3,
    "fa23-cs425-7804.cs.illinois.edu": 4,
    "fa23-cs425-7805.cs.illinois.edu": 5,
    "fa23-cs425-7806.cs.illinois.edu": 6,
    "fa23-cs425-7807.cs.illinois.edu": 7,
    "fa23-cs425-7808.cs.illinois.edu": 8,
    "fa23-cs425-7809.cs.illinois.edu": 9,
    "fa23-cs425-7810.cs.illinois.edu": 10
}


def generate_log_line():
    log_levels = ["INFO", "WARN", "ERROR", "DEBUG"]
    components = ["Database", "Server", "UI", "Auth", "Cache"]
    messages = [
        "Connection established",
         "Data fetched",
        "Cache miss",
        "User authenticated",
        "Connection closed",
        "Query executed",
        "Error encountered",
        "Data updated",
        "Transaction commit",
    ]

    log_level = random.choice(log_levels)
    component = random.choice(components)
    message = random.choice(messages)
    timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())

    return f"{timestamp} - {component} - {log_level} - {message}"


def generate_log(path):
    log_lines = [generate_log_line() for _ in range(10000)]

    for _ in range(10):
        timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
        log_lines.append(f'{timestamp} - TEST - Unit Test - Known Uncommon Line')
        
    for _ in range(1000):
        timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
        log_lines.append(f'{timestamp} - TEST - Unit Test - Known Common Line')
        # Save log lines to a file
    with open(path, 'w+') as f:
        for line in log_lines:
            f.write(f"{line}\n")


def main() -> None:
    path = f'/cs425/test.machine.{ _VMS.get(socket.gethostname())}.log'
    os.makedirs(os.path.dirname(path), exist_ok=True)
    generate_log(path)


main()
