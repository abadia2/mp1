from flags import Flags

PORT = 420_69  # non-privileged ports are > 1023

# Message Structure (in bytes)
MSG_TYPE = 1
MSG_LENGTH = 2
BODY = 256 - MSG_TYPE - MSG_LENGTH
MESSAGE_SIZE = MSG_TYPE + MSG_LENGTH + BODY

# Parsing Message
MSG_TYPE_START = 0
MSG_LENGTH_START = MSG_TYPE
MSG_BODY_START = MSG_LENGTH_START + MSG_LENGTH
ENCODE_FORMAT = "utf-8"


def msg_flags_to_bytes(msg_type: Flags) -> bytes:
    return bytes([msg_type.value])


def msg_flags_from_bytes(_bytes: bytes) -> int:
    return int.from_bytes(_bytes[:MSG_TYPE], byteorder='big')


def msg_length_to_bytes(_len: int) -> bytes:
    return _len.to_bytes(2, byteorder="big")


def msg_length_from_bytes(_bytes: bytes) -> int:
    return int.from_bytes(_bytes[MSG_LENGTH_START:MSG_BODY_START], byteorder="big")


def msg_body_to_bytes(contents: str) -> bytes:
    _bytes = contents.encode(ENCODE_FORMAT)
    if BODY - len(_bytes) < 0:
        raise Exception("Body is too big")
    return _bytes + b' ' * (BODY - len(_bytes))


def msg_body_from_bytes(_bytes: bytes, msg_length) -> str:
    return _bytes[MSG_BODY_START:MSG_BODY_START + msg_length].decode(ENCODE_FORMAT)


def create_msg_from_bytes(msg_type: Flags, msg_body: bytes = b''):
    msg_length: int = len(msg_body)
    return msg_flags_to_bytes(msg_type) + msg_length_to_bytes(msg_length) + msg_body + b' ' * (BODY - msg_length)


def create_msg_from_string(msg_type: Flags, msg_body: str = ""):
    msg_length: int = len(msg_body.encode(ENCODE_FORMAT))
    return msg_flags_to_bytes(msg_type) + msg_length_to_bytes(msg_length) + msg_body_to_bytes(msg_body)

