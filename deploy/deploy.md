# Deploy Guide

**The deploy scripts will not work on VMs**

The deploy scripts are designed to be run from your local machine to manage the Socket Servers on the VMs.

_Note_: All commands are Windows based

## Deploy Scripts: Overview

  1. Choose either "SSH Authentication" or "Password Authentication"
  2. Run those commands to deploy and start all Socket Servers for the VMs

### SSH Authentication
```shell
# 1. Download demo logs on all VMs
python deploy/deploy_logs.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425

# 2. Clones mp1 from GitLab or pulls updates the existing mp1 with the latest version from Gitlab
python deploy/deploy_mp1.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425

# 3. Activate Socket Servers on all VMs
python deploy/activate.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425
```

### Password Authentication
```shell
# 1. Download demo logs on all VMs
python deploy/deploy_logs.py lakshay3 --password <LDAP Password>

# 2. Clones mp1 from GitLab or pulls updates the existing mp1 with the latest version from Gitlab
python deploy/deploy_mp1.py lakshay3 --password <LDAP Password>

# 3. Activate Socket Servers on all VMs
python deploy/activate.py lakshay3 --password <LDAP Password>
```

## Deploy Scripts: In-Depth

Read this if you want to know more about all the deployment scripts.

### 1. Load Demo Logs

Instead of manually SSH-ing into each VM and downloading the demo logs, use `deploy_mp1.py` which fetches the demo logs 
from https://github.com/Alex-Badia/mp1-demo-logs and downloads the `vm#.log` that
corresponds to the `VM#`. Works for both SSH key authentication and password authentication.

_Note_: These log files are 50K-60KB, so the download can take a good minute.

```shell
# SSH Key Authentication and downloads demo logs on all VMs
python deploy/deploy_logs.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425

# SSH Key Authentication and downloads demo logs for only VM1, VM2, and VM3
python deploy/deploy_logs.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425 --vms fa23-cs425-7801.cs.illinois.edu fa23-cs425-7802.cs.illinois.edu fa23-cs425-7803.cs.illinois.edu

# Password Authentication and downloads demo logs on all VMs
python deploy/deploy_logs.py lakshay3 --password <LDAP Password> 

# Password Authentication and downloads demo logs for only VM1, VM2, and VM3
python deploy/deploy_logs.py lakshay3 --password <LDAP Password> --vms fa23-cs425-7801.cs.illinois.edu fa23-cs425-7802.cs.illinois.edu fa23-cs425-7803.cs.illinois.edu

# Change directory the demo logs are downloaded to on the VM (Defaults to /cs425)
python deploy/deploy_logs.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425 --download_directory /tmp
```


### 2. Deploy MP1

Instead of manually SSH-ing into each VM, cloning/pulling mp1, use `deploy_mp1.py`. Works for both SSH key authentication and password authentication:

```shell
# SSH Key Authentication and deploys mp1 on all VMs
python deploy/deploy_mp1.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425

# SSH Key Authentication and deploys mp1 for only VM1, VM2, and VM3
python deploy/deploy_mp1.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425 --vms fa23-cs425-7801.cs.illinois.edu fa23-cs425-7802.cs.illinois.edu fa23-cs425-7803.cs.illinois.edu

# Password Authentication and deploys mp1 on all VMs
python deploy/deploy_mp1.py lakshay3 --password <LDAP Password> 

# Password Authentication and deploys mp1 for only VM1, VM2, and VM3
python deploy/deploy_mp1.py lakshay3 --password <LDAP Password> --vms fa23-cs425-7801.cs.illinois.edu fa23-cs425-7802.cs.illinois.edu fa23-cs425-7803.cs.illinois.edu
```

### 3. Activate and Deactivate Socket Servers

Instead of manually SSH-ing into each VM and starting/stopping the socket server, use `activate.py`  and 
`deactivate.py`. Works for both SSH key authentication and password authentication.

#### Activate

_Note_: The script doesn't care if the Socket Server is running, it just cares about running
the command that will start the socket server. This means the `activate.py` may run successfully, but the Socket Server
could fail to start for some OS-level related reason (see Deactivate note).

```shell
# SSH Key Authentication and runs socket server on all VMs
python deploy/activate.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425

# SSH Key Authentication and deploys mp1 for only VM1
python deploy/activate.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425 --vms fa23-cs425-7801.cs.illinois.edu

# Password Authentication and deploys mp1 on all VMs
python deploy/activate.py lakshay3 --password <LDAP Password> 

# Password Authentication and deploys mp1 for only VM1
python deploy/activate.py lakshay3 --password <LDAP Password> --vms fa23-cs425-7801.cs.illinois.edu
```

#### Deactivate

_Note_: It takes up to 30 seconds for the Socket Server to unbind from the port it runs on. You may have to wait
a bit for before running `activate.py` (see Activate note).

```shell
# SSH Key Authentication and runs socket server on all VMs
python deploy/activate.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425

# SSH Key Authentication and deploys mp1 for only VM1
python deploy/activate.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425 --vms fa23-cs425-7801.cs.illinois.edu

# Password Authentication and deploys mp1 on all VMs
python deploy/activate.py lakshay3 --password <LDAP Password> 

# Password Authentication and deploys mp1 for only VM1
python deploy/activate.py lakshay3 --password <LDAP Password> --vms fa23-cs425-7801.cs.illinois.edu
```

## SSH Authentication Setup

These deploy scripts assume you have SSH for all VMs. **If not**, first generate a private key and public key pair:

```shell
# Keep track of where you chose to output the key pair
ssh-keygen -o -t rsa -b 4096
```

With the public key from the generated key par (the .pub file) do the following __for all__ VMs:

  1. SSH into the VM and login via password authentication `ssh -t fa23-cs425-78[xx].cs.illinois.edu`
  2. In the VM, make `/home/[user]/.ssh` available during SSH authentication by running `chmod 700 /home/[user]/.ssh`
  3. In the VM, Create an `/home/[user]/.ssh/authorized_keys` file
  4. In the VM, paste the public key (.pub file contents) into `/home/[user]/.ssh/authorized_keys` file
  5. In the VM, Make `authorized_keys` file available during SSH authentication by running `chmod 600 /home/[user]/authorized_keys`

Congratulations, you should be able to SSH into any VM without having to type a password again!

## Development Environment Details
- PyCharm Community 2023.2.1
- Python 3.8.10
- Windows