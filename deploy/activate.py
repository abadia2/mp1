import argparse
from multiprocessing import Process

import paramiko


_VMS = {
    "fa23-cs425-7801.cs.illinois.edu": 1,
    "fa23-cs425-7802.cs.illinois.edu": 2,
    "fa23-cs425-7803.cs.illinois.edu": 3,
    "fa23-cs425-7804.cs.illinois.edu": 4,
    "fa23-cs425-7805.cs.illinois.edu": 5,
    "fa23-cs425-7806.cs.illinois.edu": 6,
    "fa23-cs425-7807.cs.illinois.edu": 7,
    "fa23-cs425-7808.cs.illinois.edu": 8,
    "fa23-cs425-7809.cs.illinois.edu": 9,
    "fa23-cs425-7810.cs.illinois.edu": 10
}

_PORT = 22


def fetch_demo_logs(ssh_host: str, ssh_username: str, ssh_key: str, ssh_password: str) -> None:
    ssh_client = None
    try:
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        if ssh_key is not None:
            ssh_client.connect(ssh_host, _PORT, ssh_username, key_filename=ssh_key)
        else:
            ssh_client.connect(ssh_host, _PORT, ssh_username, ssh_password)

        _, stdout, stderr = ssh_client.exec_command(f"python3 /home/abadia2/mp1/server.py /cs425/vm{_VMS.get(ssh_host)}.log --port 42069")
    except Exception as e:
        print(f"[{ssh_host}] An error occurred: {e}")
    finally:
        if ssh_client is not None:
            ssh_client.close()


def main():
    # Command line
    parser = argparse.ArgumentParser(description="Distributed Demo Log Loader")
    parser.add_argument("username", type=str, help="UIUC LDAP netid")
    parser.add_argument("--password", type=str, help="UIUC LDAP password")
    parser.add_argument("--ssh_key", type=str, help="Absolute path to secret key for SSH")
    parser.add_argument("--vms", type=str, nargs="+", help="List of virtual machine names")
    args = parser.parse_args()
    username = args.username
    password = args.password or None
    ssh_key = args.ssh_key or None
    vms = args.vms or _VMS

    # Load demo logs from each VM in a separate process
    processes = [Process(target=fetch_demo_logs, args=(vm, username, ssh_key, password)) for vm in vms]
    _ = [process.start() for process in processes]
    _ = [process.join() for process in processes]


if __name__ == "__main__":
    main()
