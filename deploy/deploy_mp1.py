import argparse
from multiprocessing import Process

import paramiko

_BASE_DIR = "/cs425"

_VMS = {
    "fa23-cs425-7801.cs.illinois.edu": 1,
    "fa23-cs425-7802.cs.illinois.edu": 2,
    "fa23-cs425-7803.cs.illinois.edu": 3,
    "fa23-cs425-7804.cs.illinois.edu": 4,
    "fa23-cs425-7805.cs.illinois.edu": 5,
    "fa23-cs425-7806.cs.illinois.edu": 6,
    "fa23-cs425-7807.cs.illinois.edu": 7,
    "fa23-cs425-7808.cs.illinois.edu": 8,
    "fa23-cs425-7809.cs.illinois.edu": 9,
    "fa23-cs425-7810.cs.illinois.edu": 10
}

_PORT = 22


def fetch_demo_logs(ssh_host: str, ssh_username: str, ssh_key: str, ssh_password: str) -> None:
    ssh_client = None
    try:
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        if ssh_key is not None:
            ssh_client.connect(ssh_host, _PORT, ssh_username, key_filename=ssh_key)
        else:
            ssh_client.connect(ssh_host, _PORT, ssh_username, ssh_password)

        # Check if the mp1 folder already exists
        _, stdout, stderr = ssh_client.exec_command(f"test -e {_BASE_DIR}/mp1 && echo 1 || echo 0")
        mp1_exists = int(stdout.read().decode().strip())

        if mp1_exists == 0:
            _, stdout, stderr = ssh_client.exec_command(f"cd {_BASE_DIR} && git clone git@gitlab.engr.illinois.edu:abadia2/mp1.git")
            git_clone_ssh_output = stdout.read().decode()
            git_clone_ssh_error = stderr.read().decode()

            if git_clone_ssh_error:
                print(f"[{ssh_host}] Error running git clone git@gitlab.engr.illinois.edu:abadia2/mp1.git: {git_clone_ssh_error}")
            else:
                print(f"[{ssh_host}] Cloned pulled mp1 repository")
        else:
            print(f"[{ssh_host}] mp1 already exists")
            _, stdout, stderr = ssh_client.exec_command(f"cd {_BASE_DIR}/mp1 && git reset --hard && git fetch && git pull -f")
            git_force_pull_output = stdout.read().decode()
            git_force_pull_error = stderr.read().decode()
            if git_force_pull_error:
                print(f"[{ssh_host}] Error running 'git pull -f': {git_force_pull_error}")
            else:
                print(f"[{ssh_host}] Force pulled mp1 repository")

        # Upgrade pip and install requirements
        ssh_client.exec_command(f"sudo pip3 install --upgrade pip")
        _, stdout, stderr = ssh_client.exec_command(f"sudo pip3 install -r {_BASE_DIR}/mp1/requirements.txt")

        # Mark the downloaded repository as chmod 777
        chmod_command = f"sudo chmod -R 777 {_BASE_DIR}"
        _, stdout, stderr = ssh_client.exec_command(chmod_command)
        chmod_output = stdout.read().decode()
        chmod_error = stderr.read().decode()

        if chmod_error:
            print(f"[{ssh_host}] Error running chmod: {chmod_error}")
        else:
            print(f"[{ssh_host}] File permissions changed to chmod 777")

    except Exception as e:
        print(f"[{ssh_host}] An error occurred: {str(e)}")
    finally:
        if ssh_client is not None:
            ssh_client.close()


def main():
    # Command line
    parser = argparse.ArgumentParser(description="Distributed Demo Log Loader")
    parser.add_argument("username", type=str, help="UIUC LDAP netid")
    parser.add_argument("--password", type=str, help="UIUC LDAP password")
    parser.add_argument("--ssh_key", type=str, help="Absolute path to secret key for SSH")
    parser.add_argument("--vms", type=str, nargs="+", help="List of virtual machine names")
    args = parser.parse_args()
    username = args.username
    password = args.password or None
    ssh_key = args.ssh_key or None
    vms = args.vms or _VMS

    # Load demo logs from each VM in a separate process
    processes = [Process(target=fetch_demo_logs, args=(vm, username, ssh_key, password)) for vm in vms]
    _ = [process.start() for process in processes]
    _ = [process.join() for process in processes]


if __name__ == "__main__":
    # abadia2 --ssh_key C:\Users\18454\.ssh\id_rsa_cs_425
    main()
