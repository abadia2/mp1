import argparse
from multiprocessing import Process

import paramiko


_PORT = 22
_VMS = {
    "fa23-cs425-7801.cs.illinois.edu": 1,
    "fa23-cs425-7802.cs.illinois.edu": 2,
    "fa23-cs425-7803.cs.illinois.edu": 3,
    "fa23-cs425-7804.cs.illinois.edu": 4,
    "fa23-cs425-7805.cs.illinois.edu": 5,
    "fa23-cs425-7806.cs.illinois.edu": 6,
    "fa23-cs425-7807.cs.illinois.edu": 7,
    "fa23-cs425-7808.cs.illinois.edu": 8,
    "fa23-cs425-7809.cs.illinois.edu": 9,
    "fa23-cs425-7810.cs.illinois.edu": 10
}


def fetch_demo_logs(ssh_host: str, ssh_username: str, ssh_key: str, ssh_password: str, download_directory: str) -> None:
    ssh_client = None
    try:
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        if ssh_key is not None:
            ssh_client.connect(ssh_host, _PORT, ssh_username, key_filename=ssh_key)
        else:
            ssh_client.connect(ssh_host, _PORT, ssh_username, ssh_password)

        # Check if the file already exists in
        log_filename = f"vm{_VMS.get(ssh_host)}.log"
        stdin, stdout, stderr = ssh_client.exec_command(f"test -e /{download_directory}/{log_filename} && echo 1 || echo 0")
        file_exists = int(stdout.read().decode().strip())

        if file_exists == 0:
            log_url = f"https://raw.githubusercontent.com/Alex-Badia/mp1-demo-logs/main/{log_filename}"
            wget_command = f"wget -O /{download_directory}/{log_filename} {log_url}"
            stdin, stdout, stderr = ssh_client.exec_command(wget_command)
            wget_output = stdout.read().decode()
            wget_error = stderr.read().decode()

            if wget_error:
                print(f"[{ssh_host}] Error running wget: {wget_error}")
            else:
                print(f"[{ssh_host}] Demo log file downloaded: {log_url}")
        else:
            print(f"[{ssh_host}] File {log_filename} already exists in /{download_directory}")

        # Mark the downloaded demo log file as chmod 777
        chmod_command = f"sudo chmod -R 777 /{download_directory}/{log_filename}"
        stdin, stdout, stderr = ssh_client.exec_command(chmod_command)
        chmod_output = stdout.read().decode()
        chmod_error = stderr.read().decode()

        if chmod_error:
            print(f"[{ssh_host}] Error running chmod: {chmod_error}")
        else:
            print(f"[{ssh_host}] File permissions changed to chmod 777")

    except Exception as e:
        print(f"[{ssh_host}] An error occurred: {str(e)}")
    finally:
        if ssh_client is not None:
            ssh_client.close()


def main():
    # Command line
    parser = argparse.ArgumentParser(description="Distributed Demo Log Loader")
    parser.add_argument("username", type=str, help="UIUC LDAP netid")
    parser.add_argument("--password", type=str, help="UIUC LDAP password")
    parser.add_argument("--ssh_key", type=str, help="Absolute path to secret key for SSH")
    parser.add_argument("--download_directory", type=str, help="Directory to store the log file")
    parser.add_argument("--vms", type=str, nargs="+", help="List of virtual machine names")
    args = parser.parse_args()
    username = args.username
    password = args.password or None
    ssh_key = args.ssh_key or None
    download_directory = args.download_directory or "cs425"
    vms = args.vms or _VMS

    # Load demo logs from each VM in a separate process
    processes = [Process(target=fetch_demo_logs, args=(vm, username, ssh_key, password, download_directory))for vm in vms]
    _ = [process.start() for process in processes]
    _ = [process.join() for process in processes]


if __name__ == "__main__":
    # python deploy_logs.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425
    main()
