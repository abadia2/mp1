import math
import subprocess
import time
import unittest
from multiprocessing import Process, Queue

import paramiko

from client import fetch_distributed_logs

_BASE_DIR = "/cs425"
_PORT = 42090

_VMS = [
    "fa23-cs425-7801.cs.illinois.edu",
    "fa23-cs425-7803.cs.illinois.edu",
    "fa23-cs425-7804.cs.illinois.edu",
    "fa23-cs425-7805.cs.illinois.edu",
    "fa23-cs425-7806.cs.illinois.edu",
    "fa23-cs425-7807.cs.illinois.edu",
    "fa23-cs425-7808.cs.illinois.edu",
    "fa23-cs425-7809.cs.illinois.edu",
    "fa23-cs425-7810.cs.illinois.edu"
]

_VMS_MAP = {
    "fa23-cs425-7801.cs.illinois.edu": 1,
    "fa23-cs425-7803.cs.illinois.edu": 3,
    "fa23-cs425-7804.cs.illinois.edu": 4,
    "fa23-cs425-7805.cs.illinois.edu": 5,
    "fa23-cs425-7806.cs.illinois.edu": 6,
    "fa23-cs425-7807.cs.illinois.edu": 7,
    "fa23-cs425-7808.cs.illinois.edu": 8,
    "fa23-cs425-7809.cs.illinois.edu": 9,
    "fa23-cs425-7810.cs.illinois.edu": 10
}


def load_test_logs(vm_id):
    ssh_commands = [
        ['ssh', '-t', vm_id, f'python3 {_BASE_DIR}/mp1/log_gen.py'],
        ['ssh', '-t', vm_id, f"sudo chmod -R 777 {_BASE_DIR}"],
    ]
    for ssh_command in ssh_commands:
        subprocess.run(ssh_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


def remove_test_logs(vm_id):
    subprocess.run(
        ['ssh', '-t', vm_id, f'rm /cs425/test.machine.{_VMS_MAP.get(vm_id)}.log'],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )


def activate_socket_servers(ssh_client, vm_id):
    _, stdout, stderr = ssh_client.exec_command(
        f"python3 /cs425/mp1/server.py /cs425/test.machine.{_VMS_MAP.get(vm_id)}.log --port {_PORT}")


def deactivate_socket_servers(ssh_client, vm_id):
    _, stdout, stderr = ssh_client.exec_command(f"fuser {_PORT}/tcp -k")


def setup_vm(vm_id):
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(vm_id, 22, "abadia2", key_filename=r"C:\Users\18454\.ssh\id_rsa_cs_425")
    load_test_logs(vm_id)
    activate_socket_servers(ssh_client, vm_id)
    print(f"[{vm_id}] Waiting for test socket server to start up...")
    time.sleep(30)


def teardown_vm(vm_id):
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(vm_id, 22, "abadia2", key_filename=r"C:\Users\18454\.ssh\id_rsa_cs_425")
    remove_test_logs(vm_id)
    deactivate_socket_servers(ssh_client, vm_id)
    print(f"[{vm_id}] Waiting for test socket server to shutdown...")
    time.sleep(30)


def test_query(query):
    result_queue: Queue = Queue()
    default_args = (5, result_queue, _PORT)
    processes = [Process(target=fetch_distributed_logs, args=(query, vm, *default_args)) for vm in _VMS]
    for process in processes:
        process.start()
    for process in processes:
        process.join()

    total = 0
    while not result_queue.empty():
        result = result_queue.get()
        line_count = float(result.get("line_count"))
        if not math.isnan(line_count):
            total += line_count

    return total


class TestDistributedLogs(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        processes = [Process(target=setup_vm, args=(vm,)) for vm in _VMS]
        for process in processes:
            process.start()
        for process in processes:
            process.join()

    @classmethod
    def tearDownClass(cls) -> None:
        processes = [Process(target=teardown_vm, args=(vm,)) for vm in _VMS]
        for process in processes:
            process.start()
        for process in processes:
            process.join()

    def test_common_line(self):
        actual = test_query('grep "Known Common Line"')
        self.assertEqual(actual, 9000)

    def test_uncommon_line(self):
        actual = test_query('grep "Known Uncommon Line"')
        self.assertEqual(actual, 90)

    def test_regex_words_starting_with_c(self):
        words_starting_with_known = r"\<Known\w*"
        actual = test_query(f'grep -E "{words_starting_with_known}"')
        self.assertGreater(actual, 0)


if __name__ == '__main__':
    unittest.main()
