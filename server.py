import argparse
import math
import socket
import subprocess
import threading
from datetime import datetime
from typing import Generator, Union, Tuple

from message import (BODY, create_msg_from_bytes, create_msg_from_string, MESSAGE_SIZE, msg_body_from_bytes,
                     msg_flags_from_bytes, msg_length_from_bytes, PORT)
from flags import Flags, is_flag_set


def chunk_log_line(byte_line: bytes, byte_line_length: int) -> Generator[Union[bytes, int], any, None]:
    for i in range(0, byte_line_length, BODY):
        yield byte_line[i:i+BODY]


def send_file(conn: socket.socket, log_file_path: str) -> int:
    with open(log_file_path, "rb") as log_file:
        line_count = 0
        for byte_line in log_file:
            line_count += 1
            for byte_chunk in chunk_log_line(byte_line, len(byte_line)):
                conn.send(create_msg_from_bytes(Flags.LOG_RESPONSE, byte_chunk))
    return line_count


def grep(grep_command: str, log_file_path: str) -> str:
    tmp_file: str = f"/tmp/{datetime.now().strftime('%Y-%m-%d-%H.%M.%S')}-{threading.currentThread().name}.txt"
    subprocess.run(f"{grep_command.strip()} {log_file_path} > {tmp_file}", shell=True, check=True)
    return tmp_file


def disconnect(conn: socket.socket, line_count) -> None:
    conn.send(create_msg_from_string(Flags.DISCONNECT, str(line_count)))


def read_socket(conn: socket.socket, buffer: bytes) -> Tuple[bytes, bytes]:
    # Read bytes from socket until a message can be read from the buffer
    while len(buffer) < MESSAGE_SIZE:
        _bytes: bytes = conn.recv(MESSAGE_SIZE)
        buffer += _bytes
    byte_msg: bytes = buffer[:MESSAGE_SIZE]
    cleared_buffer: bytes = buffer[MESSAGE_SIZE:]
    return byte_msg, cleared_buffer


def handle(conn: socket.socket, log_file_path: str) -> None:

    # Log source
    thread_id = threading.currentThread().name

    # Since the number of bytes read from the socket vary, store bytes in a buffer until a message can be read
    initial_buffer: bytes = b''

    line_count = math.nan

    while True:

        # Read socket with buffer
        byte_msg, cleared_buffer = read_socket(conn, initial_buffer)
        initial_buffer = cleared_buffer

        # First byte contains flags indicating the type of message where only one flag should be set at a time
        msg_flags: int = msg_flags_from_bytes(byte_msg)

        # If multiple flags are set, be careful of the order
        if is_flag_set(msg_flags, Flags.LOG_REQUEST):
            try:
                msg_length: int = msg_length_from_bytes(byte_msg)
                grep_command: str = msg_body_from_bytes(byte_msg, msg_length)
                print(f"[{thread_id}] Executing grep query: {grep_command}")
                result = grep(grep_command, log_file_path)
                line_count = send_file(conn, result)
            except Exception as e:
                print(f"[{thread_id}] {e}")
            finally:
                print(f"[{thread_id}] Finished, notifying client")
                disconnect(conn, line_count)
        elif is_flag_set(msg_flags, Flags.DISCONNECT):
            print(f"[{thread_id}] Client disconnected")
            break

    conn.close()


def start() -> None:
    # Command line
    parser = argparse.ArgumentParser(description="Distributed Log Grep Socket Server")
    parser.add_argument("path", help="Path to the server's log file")
    parser.add_argument("--port", help="Override the default port the server is running on")
    args = parser.parse_args()
    log_file_path = args.path
    port = args.port or PORT

    _HOST = socket.gethostbyname(socket.gethostname())
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((_HOST, int(port)))

    print(f"[{_HOST}] Server started")
    s.listen()
    print(f"[{_HOST}] Server is listening on {port}...")

    try:
        while True:
            connection, address = s.accept()
            thread = threading.Thread(target=handle, args=(connection, log_file_path))
            thread.start()
    except Exception as e:
        print(e)
    finally:
        s.close()


if __name__ == "__main__":
    start()
