# Distributed Grep

Assuming the Socket Servers are deployed and activated on VMs, to use Distributed Grep:

```shell
# Run the client script:
#   python client.py "['grep [options] [pattern]']" --timeout 5 <int> --vms [machine name] [machine name] ... --port [int]
#  
# If Socket Server is running on VMs

# Distributed Grep on all logs (Defaults to all VMs without --vms argument):
python client.py <grep [options] "pattern">

# Use --vms to pick machines
#
# Distributed Grep on the Socket Server running locally and the Socket Servers on VM 1, VM 3, VM 4
# python client.py <grep regex> --timeout 5 --vms 10.193.90.61 fa23-cs425-7801.cs.illinois.edu fa23-cs425-7802.cs.illinois.edu fa23-cs425-7803.cs.illinois.edu
python client.py "grep GET" --timeout 5 --vms fa23-cs425-7810.cs.illinois.edu fa23-cs425-7801.cs.illinois.edu fa23-cs425-7803.cs.illinois.edu fa23-cs425-7804.cs.illinois.edu
```


## Development Guide

### Development with VMs

#### Push to Gitlab Approach:
  1. Makes change locally
  2. Push changes to Gitlab
  3. Run `deploy/deploy_mp1.py` to get changes on all VMs
     - _See `deploy/deploy.md` to learn how to deploy Socket Servers to VMs._
  4. Restart the Socket Servers on all VMs by running 
      1. `deploy/deactivate.py`
      2. `deploy/activate.py`

#### Quick Change Approach (Recommended):
  1. Make changes locally
  2. SSH into VM and make corresponding changes there
  3. Kill Socket Server:
      - `fuser 42069/tcp -k` 
  4. Start Socket Server:
      - Replace `#` with the VM number when running this: `python3 server.py /cs425/vm#.log`
  5. Run `client.py`, but only target the VM with the Socket Server that you made changes to 
      - `python client.py <grep query string> --vms <machine that has changes>`

Once you are ready to push changes then:

```shell
# 2. Clones mp1 from GitLab or pulls updates the existing mp1 with the latest version from Gitlab
python deploy/deploy_mp1.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425

# 3. Activate Socket Servers on all VMs
python deploy/deactivate.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425

# 3. Activate Socket Servers on all VMs
python deploy/activate.py abadia2 --ssh_key C:/Users/18454/.ssh/id_rsa_cs_425
```

## Local Development

### Socket Server

Everytime you make changes to the Socket Server, you must restart it locally:
```shell
# Run the Socket Server:
#   python server.py /path/to/log-file.log
python server.py /path/to/logs
```

### Client

To run the client after making changes:

```shell
# Run the client script:
#   python client.py <grep query string> --timeout 5 <int> --vms <ip | machine name> <ip | machine name> ...
#  
# If local server is running at 10.193.90.61
python client.py <grep [options] "pattern"> --timeout 5 --vms 10.193.90.61
```

## Unit Test

```shell
python test.py
```
